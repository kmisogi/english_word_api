#!/usr/bin/env python
#! -*- coding:utf-8 -*-

""" 英単語の品詞情報を取得する。
"""

import re
import treetaggerwrapper as ttw

def main():
    # TAGDIRを指定する必要がある。
    tagger = ttw.TreeTagger(TAGLANG='en',TAGDIR='/usr/local/src/treetagger')
    lines = read_file()
    result = []
    dict_def = load_def()
    for line in lines:
        try:
            english, japanese = line.split('\t')
            japanese = japanese.rstrip('\n')
            # 複数語は取り込み対象外とする。
            if re.match('.+[\s　].+', english):
                continue
            tags = tagger.TagText(english)
            tags2 = ttw.make_tags(tags)
            word_type = tags2[0][1]
            result.append(english + '\t' + japanese + '\t' + dict_def[word_type])
        except:
            continue
    write_file(result)

def read_file():
    """
    パブリックドメインの英語辞書データ
    https://kujirahand.com/web-tools/EJDictFreeDL.php
    """
    with open('./ejdic-hand-utf8.tsv') as f:
        return f.readlines()

def write_file(ary):
    with open('./ejdic-hand-utf8-with-word-type.tsv', 'w') as f:
        return f.write('\n'.join(ary))

def load_def():
    """
    TreeTaggerの付与する品詞タグを、実際の品詞表記に変換するための定義ファイル
    http://computer-technology.hateblo.jp/entry/20150824/p1
    """
    result = {}
    with open('./def_word_type.tsv') as f:
        for line in f.readlines():
            list_line = line.split('\t')
            result[list_line[0]] = list_line[3].rstrip('\n')
    return result

if __name__ == '__main__':
    main()
