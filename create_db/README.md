create_db

# Description
英単語APIのDB構築

# Files
 - def_word_type.tsv
     - TreeTaggerの付与する品詞タグを、実際の品詞表記に変換するための定義ファイル
     - 日本語訳はこちらを参考にさせて頂きました: http://computer-technology.hateblo.jp/entry/20150824/p1
 - ejdic-hand-utf8.tsv
     - パブリックドメインの英和辞書データ (ejdic-hand)から、以下の処理を行った辞書です。
         - 一部不要語（複数語など）の削除
         - UTF-8への変換
     - 元データはこちらから取得させて頂きました: https://kujirahand.com/web-tools/EJDictFreeDL.php
 - ejdic-hand-utf8-with-word-type.tsv
     - get_word_type.pyの生成ファイルで、ejdic-hand-utf8.tsvに品詞情報も付与した辞書です。
 - get_word_type.py
     - ejdic-hand-utf8.tsvに品詞情報を付与するスクリプトです。
